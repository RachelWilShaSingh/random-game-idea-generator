$( document ).ready( function() {
            var genres = [
                "TOURNAMENT FIGHTER",
                "SIDESCROLLING PLATFORMER",
                "PUZZLE PLATFORMER",
                "SHMUP",
                "JAPANESE-STYLE RPG",
                "CRPG",
                "ACTION RPG",
                "POINT-AND-CLICK ADVENTURE",
                "THIRD PERSON ACTION RPG",
                "THIRD PERSON ACTION",
                "FIRST PERSON SHOOTER",
                "TOP-DOWN SHOOTER",
                "SIDESCROLLING BEAT-EM-UP",
                "ROGUELIKE",
                "BATTLE ROYALE",
                "HERO SHOOTER",
                "MOBA",
                "GEM MATCHING PUZZLE",
                "PUZZLE",
                "SIM",
                "TURN-BASED STRATEGY",
                "REAL-TIME STRATEGY",
                "CLASSIC ARCADE-STYLE",
                "RHYTHM GAME",
                "SOULS-LIKE",
                "VIRTUAL PET",
                "MMORPG",
                "ADVENTURE",
                "STICK SHOOTER",
                "CLICKER",
                "TOWER DEFENSE",
                "FARMING"
            ];
            
            var nouns = [
                "SOFTWARE ENGINEERS",
                "QUALITY ASSURANCE ENGINEERS",
                "POTATOES",
                "NONBINARY FOLKS",
                "CATS",
                "DRAGONS",
                "SPOOKY SKELETONS",
                "CUTE OLD GRANNIES",
                "SPACE ALIENS",
                "REALLY BORING HUMANS",
                "ARTISTS",
                "JANITORS",
                "BARISTAS",
                "GHOSTS",
                "VEGETABLES",
                "CIVIL ENGINEERS",
                "ROBOTS",
                "CUTE BIRDS",
                "DISASTER BISEXUALS",
                "GENDERFLUID TEENAGERS",
                "GEN X-ERS",
                "MILLENNIALS",
                "GEN Z-ERS",
                "LOCAL COMEDIANS",
                "PRINCESSES",
                "LINGUISTS",
                "ZOMBIES",
                "CRYPTIDS",
                "TAROT CARD READERS",
                "SAD DADS",
                "OFFICE WORKERS",
                "PRESCHOOL TEACHERS",
                "TEACHERS",
                "DAIRY PRODUCTS",
                "BAKERS",
                "HIPSTERS",
                "GRAD STUDENTS",
                "STAY-AT-HOME PARENTS",
                "MOOSE",
                "PIANISTS",
                "VOCALISTS",
                "BASSISTS",
                "TRUMPET PLAYERS",
                "&quot;SELF-TAUGHT&quot; GUITARISTS",
                "GAME DESIGNERS",
                "INSOMNIACS",
                "CUTE BUGS",
                "ASEXUALS",
                "ROLY POLY BUGS",
                "TOYS",
                "BAD VOICE ACTORS",
                "LACTOSE INTOLERANT PEOPLE",
                "WINDMILLS",
            ];
            
            var heroes = [
                "FURRIES",      // Furries get enough shit online; I'm not gonna let them be enemies in my generator
                "RETAIL WORKERS",
                "GAY DADS",
                "GAY MOMS",
            ];
            
            var enemies = [
                "LANDLORDS",
                "COVID-19",
                "EXISTENTIAL DREAD",
                "INTERNET INFLUENCERS",
                "RICH KIDS WITH THEIR OWN STARTUPS",
                "BOOMERS",
                "ANXIETY",
                "IMPERIALISM",
                "PASSIVE-AGGRESSIVE PARENTS",
                "STUDENT LOANS",
                "PEOPLE WHO DISLIKE CATS",
                "THE PATRIARCHY",
            ];
            
            function GetRandomGenre() {
                return genres[ Math.floor( Math.random() * genres.length ) ];
            }
            
            function GetRandomEnemy() {
                var AorB = Math.floor( Math.random() * 4 );
                if ( AorB == 0 )
                {
                    return enemies[ Math.floor( Math.random() * enemies.length ) ];
                }
                else
                {
                    return nouns[ Math.floor( Math.random() * nouns.length ) ];
                }
            }
            
            function GetRandomHero() {
                var AorB = Math.floor( Math.random() * 4 );
                if ( AorB == 0 )
                {
                    return heroes[ Math.floor( Math.random() * heroes.length ) ];
                }
                else
                {
                    return nouns[ Math.floor( Math.random() * nouns.length ) ];
                }
            }
            
            function GenerateIdea() {
                var str = "A <em>" + 
                    GetRandomGenre() + "</em> game with <em>" +
                    GetRandomHero() + "</em> as heroes and <em>" +
                    GetRandomEnemy() + "</em> as enemies.";
                
                $( "#game-idea" ).html( str );
            }
            
            $( "#generate" ).click( function() {
                GenerateIdea();
            } );
            
            GenerateIdea();
        } );
